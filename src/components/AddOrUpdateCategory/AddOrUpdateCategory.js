import React from 'react';
import {connect} from 'react-redux';
import axios from '../../axios-primary';

import * as actions from '../../store/actions';
import Modal from "../UI/Modal/Modal";
import {getFormDataValues, updateObject} from "../../shared/utility";
import Input from "../UI/Input/Input";

class AddOrUpdateCategory extends React.Component {
    state = {
        formControls: {
            title: {
                label: 'Title',
                type: 'text',
                value: ''
            }
            /*parentId: {
                label: 'Parent',
                type: 'select',
                value: '',
                options: []
            }*/
        }
    };

    // NOTE: state depended on props change example
    /*static getDerivedStateFromProps(nextProps, prevState) {
        console.log(nextProps);
        const updateControls = updateObject(prevState.formControls, {
            title: updateObject(prevState.formControls['title'], {
                value: nextProps.nodeTitle
            })
        });
        return {...prevState, formControls: updateControls};
        /!*if (nextProps.categories.length !== prevState.formControls.parentId.options.length) {
            // update parentId dropdown options
            const updatedControls = updateObject(prevState.formControls, {
                parentId: updateObject(prevState.formControls['parentId'], {
                    options: nextProps.categories
                })
            });
            return {...prevState, formControls: updatedControls};
        }
        return prevState;*!/
    }*/

    onSubmit = e => {
        e.preventDefault();
        const categoryData = getFormDataValues(this.state.formControls);
        categoryData.parentId = this.props.parentId;
        if (this.props.nodeId) {
            categoryData.id = this.props.nodeId;
            this.props.onUpdateLoading();
            axios.put('/category', categoryData)
                .then(res => {
                    this.props.onNodeUpdated({_id: res.data._id, title: res.data.title});
                    this.props.onUpdateLoading(false);
                });

            // NOTE: chaining dispatch methods example
            /*this.props.onUpdateCategory(categoryData)
                .then(res => {
                    this.props.onNodeUpdated(categoryData)
                })*/
        } else {
            console.log(categoryData, 'categoryData');
            this.props.onUpdateLoading();
            axios.post('/category', categoryData)
                .then(res => {
                    this.props.onNodeCreated({_id: res.data._id, title: res.data.title});
                    this.props.onUpdateLoading(false);
                });
        }
    };

    onChange = (e, key) => {
        const updatedControls = updateObject(this.state.formControls, {
            [key]: updateObject(this.state.formControls[key], {
                value: e.target.value
            })
        });

        this.setState({formControls: updatedControls});
    };

    render() {
        const formElementsArray = [];
        for (let key in this.state.formControls) {
            formElementsArray.push({
                key: key,
                config: this.state.formControls[key]
            });
        }

        const form = formElementsArray.map(formElement => (
            <Input
                key={formElement.key}
                label={formElement.config.label}
                options={formElement.config.options}
                itemKey={formElement.key}
                elemType={formElement.config.type}
                shouldValidate={formElement.config.validators}
                value={formElement.config.value}
                valid={formElement.config.isValid}
                touched={formElement.config.isTouched}
                changed={e => this.onChange(e, formElement.key)}
            />
        ));

        return (
            <div>
                <Modal show={this.props.addingCategory}
                       title={'Add Category'}
                       onModalClose={this.props.toggleModal}
                       onModalAccept={e => this.onSubmit(e)}>
                    <div className="masonry-item">
                        <div className="bgc-white p-20 bd">
                            <form>
                                {form}
                            </form>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        error: state.global.error
        // NOTE: reselect example
        // categories: getCategoriesForSelect(state)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onUpdateLoading: (loading = false, error = null) => dispatch(actions.updateLoading(loading, error)),
        // NOTE: if you want to chain dispatch actions, use promises
        /*onAddCategory: payload => {
            return new Promise((resolve, reject) => {
                dispatch(actions.addCategory(payload));
                resolve();
            });
        },
        onUpdateCategory: payload => {
            return new Promise((resolve, reject) => {
                dispatch(actions.updateCategory(payload));
                resolve();
            });
        }*/
    };
};

// NOTE: reselect example
/*const getCategories = state => state.category.categories;
const getCategoriesForSelect = createSelector(getCategories, (categories) => {
    console.log(categories.length, 'llllll');
    if (categories && categories.length) {
        const categoriesSelect = categories.map(item => {
            return {
                value: item._id,
                label: item.title
            }
        });
        categoriesSelect.unshift({value: '', label: ''});

        return categoriesSelect;
    }
    return [];
});*/

export default connect(mapStateToProps, mapDispatchToProps)(AddOrUpdateCategory);
