import React from 'react';
import moment from 'moment';

import {getFormDataValues, getFormElementsArray, updateFormOnChange} from "../../shared/utility";
import Input from "../UI/Input/Input";

class DynamicForm extends React.Component {
    state = {formControls: {}};

    static getDerivedStateFromProps(nextProps, prevState) {
        let initialState = Object.keys(nextProps.formControls).reduce((formControl, key) => {
            const defaultValues = nextProps.defaultValues;
            const prevFormControl = prevState.formControls[key] ? prevState.formControls[key] : null;
            const valueType = nextProps.formControls[key].type;

            const value = prevFormControl && prevFormControl.value ? prevFormControl.value
                : defaultValues && defaultValues[key] ? defaultValues[key] : valueType === 'date' ? moment() : '';

            formControl[key] = {
                value: value,
                errors: prevFormControl && prevFormControl.errors ? prevFormControl.errors : [],
                isTouched: !!value
            };
            return formControl;
        }, {});
        const stateData = {formControls: initialState};
        return {
            ...stateData
        }
    }

    onSubmit = (e) => {
        e.preventDefault();
        const formData = getFormDataValues(this.state.formControls);
        this.props.onSubmit(formData);
    };

    onChange = (e, key, type) => {
        const value = type === 'date' ? e : e.target.value;
        const updatedControls = updateFormOnChange(this.state.formControls, this.props.formControls, value, key, type);
        this.setState({formControls: updatedControls});
    };

    render() {
        const formElementsArray = getFormElementsArray(this.props.formControls);

        const form = formElementsArray.map(formElement => (
            <Input
                key={formElement.key}
                options={formElement.config.options}
                itemKey={formElement.key}
                elemType={formElement.config.type}
                value={this.state.formControls[formElement.key].value}
                shouldValidate={formElement.config.validators}
                errorMessages={formElement.config.errorMessages}
                errors={this.state.formControls[formElement.key].errors}
                touched={this.state.formControls[formElement.key].isTouched}
                label={formElement.config.label}
                changed={e => this.onChange(e, formElement.key, formElement.config.type)}
            />
        ));

        return (
            <>
                <h2 className="auth-section__heading-secondary text-center">{this.props.formTitle}</h2>
                <form onSubmit={e => this.onSubmit(e)}>
                    {form}
                    <div>
                        <button type="submit" className="btn btn-auth">Submit</button>
                    </div>
                    {this.props.children}
                </form>
            </>
        );
    }
}

export default DynamicForm;
