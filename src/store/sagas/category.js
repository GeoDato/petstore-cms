import {put} from 'redux-saga/effects';
import axios from '../../axios-primary';
import * as actions from '../actions';

export function* getCategories(action) {
    try {
        yield put(actions.updateLoading());

        const response = yield axios.get('/category');

        yield put(actions.getCategoriesSucceeded(response.data));
        yield put(actions.updateLoading(false));
    } catch (err) {
        yield put(actions.updateLoading(false, err));
    }
}
